<?php

return array(
    'lng.test' => 'Sample text',
    'lng.txt.titleartist' => 'Artist',
    'lng.txt.welcome'=>'Welcome artist',
    'lng.txt.message' => 'It\'s a pleasure to have you here',
    'lng.txt.new' => 'New',
    'lng.txt.edit' => 'Edit',
    'lng.txt.list' => 'List',
    'lng.txt.view' => 'view',

//formulario
    'lng.txt.ftitle'=>'Artist',
    'lng.txt.name' => 'Name',
    'lng.txt.lastname'=>'Last name',
    'lng.txt.typeart' =>'Type of art',
    'lng.txt.experience' => 'Years of experience',
    'lng.txt.freelance' => 'I work alone',
    'lng.txt.corp'=>'I work for a company',
    'lng.txt.ok'=>'Ok',
    'lng.txt.edit'=>'Edit'
);