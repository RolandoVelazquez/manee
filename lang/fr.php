<?php

return array(
    'lng.test' => 'Exemple de texte',
    'lng.txt.titleartist' => 'Artiste',
    'lng.txt.welcome'=>'Artiste bienvenue',
    'lng.txt.message' => 'C\'est un plaisir de vous avoir ici',
    'lng.txt.new' => 'Nouveau',
    'lng.txt.edit' => 'Modifier',
    'lng.txt.list' => 'Liste',
    'lng.txt.view' => 'Vue',

//formulario
    'lng.txt.ftitle'=>'Artiste',
    'lng.txt.name' => 'Prénom',
    'lng.txt.lastname'=>'Nom de famille',
    'lng.txt.typeart' =>'Type d\'art',
    'lng.txt.experience' => 'Des années d\'expérience',
    'lng.txt.freelance' => 'Je travaille seul',
    'lng.txt.corp'=>'Je travaille pour une entreprise',
    'lng.txt.ok'=>'D\'accord',
    'lng.txt.edit'=>'modifier'
);