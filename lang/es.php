<?php

return array(
    'lng.test'=>'Texto de ejemplo',
    'lng.txt.titleartist' => 'Artista',
    'lng.txt.welcome'=>'Bienvenido artista',
    'lng.txt.message' => 'Es un placer tenerte aquí',
    'lng.txt.new' => 'Nuevo',
    'lng.txt.edit' => 'Editar',
    'lng.txt.list' => 'Listar',
    'lng.txt.view' => 'Ver',

//formulario
    'lng.txt.ftitle'=>'Artista',
    'lng.txt.name' => 'Nombre',
    'lng.txt.lastname'=>'Apellido',
    'lng.txt.typeart' =>'Tipo de arte',
    'lng.txt.experience' => 'Años de experiencia',
    'lng.txt.freelance' => 'Trabajo solo',
    'lng.txt.corp'=>'Trabajo para una compañia',
    'lng.txt.ok'=>'Listo',
    'lng.txt.edit'=>'Editar'
);